from django.forms import ModelForm
from projects.models import Project, Record
from django import forms


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ("name", "description", "owner", "image")


class SearchForm(forms.Form):
    query = forms.CharField()


class RecordForm(ModelForm):
    class Meta:
        model = Record
        fields = (
            "title",
            "albumart",
            "artist",
            "description",
        )

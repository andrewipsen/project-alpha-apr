from django.urls import path
from projects.views import (
    list_projects,
    show_project,
    create_project,
    search_project,
    list_records,
    add_record,
    show_record,
)

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("library/", list_records, name="list_records"),
    path("library/add/", add_record, name="add_record"),
    path("library/<int:id>/", show_record, name="show_record"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("search/", search_project, name="search_project"),
]

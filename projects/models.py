from django.db import models
from django.conf import settings


# Create your models here.
class Project(models.Model):
    # number = models.PositiveSmallIntegerField(null=True, blank=True)
    name = models.CharField(max_length=200)
    description = models.TextField()
    image = models.URLField(default=False)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name


class Record(models.Model):
    title = models.CharField(max_length=100)
    albumart = models.URLField(default=False)
    artist = models.CharField(max_length=50)
    description = models.TextField()
    rating = models.DecimalField(max_digits=2, decimal_places=1, null=True)
    # tracklist = models.ForeignKey(
    #     "Track",
    #     related_name="records",
    #     on_delete=models.CASCADE,
    #     default=False,
    # )

    def __str__(self):
        return self.title


class Track(models.Model):
    number = models.IntegerField()
    name = models.CharField(max_length=100)
    length = models.TimeField(default=False)
    # album = models.ForeignKey(
    #     "Record", related_name="tracks", on_delete=models.PROTECT
    # )

# Generated by Django 4.1.7 on 2023-03-13 22:14

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0003_alter_project_description"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="image",
            field=models.ImageField(default=False, upload_to=""),
        ),
    ]

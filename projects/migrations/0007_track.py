# Generated by Django 4.1.7 on 2023-03-16 00:05

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0006_record"),
    ]

    operations = [
        migrations.CreateModel(
            name="Track",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("number", models.IntegerField()),
                ("name", models.CharField(max_length=100)),
                ("length", models.TimeField(default=False)),
            ],
        ),
    ]

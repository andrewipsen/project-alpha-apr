from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project, Record
from projects.forms import ProjectForm, SearchForm, RecordForm

# from tasks.models import Task

# from tasks.models import Task
from django.contrib.auth.decorators import login_required


def list_projects(request):
    list = Project.objects.all()
    Rlist = Record.objects.all()
    prev_url = request.META.get("HTTP_REFERRER")

    context = {
        "list_of_proj": list,
        "prev-url": prev_url,
        "list_of_recs": Rlist,
    }
    return render(request, "projects/projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)

    context = {
        "project": project,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)


def search_project(request):
    if request.method == "GET":
        form = SearchForm(request.GET)
        results = []
        if form.is_valid():
            query = form.cleaned_data["query"]
            results = Project.objects.filter(name__icontains=query)
    else:
        form = SearchForm()
    context = {"form": form, "results": results}
    return render(request, "projects/search_project.html", context)


def list_records(request):
    list = Record.objects.all()

    context = {"list_of_recs": list}
    return render(request, "records/records.html", context)


def add_record(request):
    if request.method == "POST":
        form = RecordForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_records")
    else:
        form = RecordForm()

    context = {
        "form": form,
    }

    return render(request, "records/add.html", context)


def show_record(request, id):
    record = get_object_or_404(Record, id=id)

    context = {
        "record": record,
    }
    return render(request, "records/show_record.html", context)

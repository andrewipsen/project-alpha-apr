from django.contrib import admin
from projects.models import Project, Record


# Register your models here.
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ["name"]


@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    list_display = ["title"]
